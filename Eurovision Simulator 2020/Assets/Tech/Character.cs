﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Character : MonoBehaviour
{
    [Voice, Header("Voice")]
    public string voice;
    [Range(-5,5)]
    public float pitchOffset;
    [Range(-5, 5)]
    public float speedOffset;
    float bouncy;
    float wobbly;
    float speed;
    float animStart;

    public static List<Character> allCharacters;
    UnityEngine.UI.RawImage tex;
    Vector3 texBasePos;

    public static void ClearAll()
    {
        foreach(Character c in allCharacters)
        {
            c.gameObject.SetActive(false);
        }
    }

    void Awake()
    {
        tex = GetComponentInChildren<UnityEngine.UI.RawImage>();
        texBasePos = tex.transform.localPosition;
        if (allCharacters == null)
            allCharacters = new List<Character>();
        allCharacters.Add(this);
    }

    private void OnDestroy()
    {
        allCharacters = null;
    }

    public void Animate(float bouncy, float wobbly, float speed)
    {
        animStart = Time.time;
        this.bouncy = bouncy;
        this.wobbly = wobbly;
        this.speed = speed;
    }

    private void Update()
    {
        if (bouncy == 0)
            tex.transform.localPosition = Vector3.Lerp(tex.transform.localPosition, texBasePos, Time.deltaTime * 8);
        else
            tex.transform.localPosition = Vector3.up * Mathf.Abs(Mathf.Sin((Time.time - animStart) * Mathf.PI * speed * 20)) * bouncy * 45;
        if (wobbly == 0)
            tex.transform.up = Vector3.Lerp(tex.transform.up, Vector3.up, Time.deltaTime * 8);
        else
            tex.transform.localEulerAngles = new Vector3 (0,0, Mathf.Sin((Time.time - animStart) * Mathf.PI * speed * 20)) * wobbly * 45;
    }
}