﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
#if RTVOICE
using Crosstales.RTVoice;
#endif
#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class CustomHierarchy : MonoBehaviour
{

    static CustomHierarchy()
    {
        EditorApplication.hierarchyWindowItemOnGUI += HandleHierarchyWindowItemOnGUI;
    }

    private static void HandleHierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
    {
        Color backgroundColor = new Color(0.76f, 0.76f, 0.76f);
        Color selectedColor = new Color(0.2f, 0.45f, 0.76f);

        var obj = EditorUtility.InstanceIDToObject(instanceID);
        if (obj != null)
        {
            bool selected = Selection.objects.Contains(obj);
            Cutscene c = ((GameObject)obj).GetComponent<Cutscene>();
            if (c != null)
            {
                Rect offsetRect = new Rect(selectionRect);
                offsetRect.x += selectionRect.size.x * 0.7f;
                offsetRect.width = selectionRect.size.x * 0.3f;
                Rect textRect = new Rect(selectionRect);
                textRect.x += 18f;
                textRect.y -= 0;
                EditorGUI.DrawRect(textRect, selected ? selectedColor : backgroundColor);
                GUI.Label(textRect, obj.name, new GUIStyle()
                { normal = new GUIStyleState() { textColor = selected ? Color.white : new Color(0.9f,0.2f,0.2f,1f) }});
                if (GUI.Button(offsetRect, "Test"))
                {
                    Cutscene.testCutscene = c;
                    EditorApplication.isPlaying = true;
                }
            }
            else
            {
                Country co = ((GameObject)obj).GetComponent<Country>();
                Song s = ((GameObject)obj).GetComponent<Song>();
                if (s)
                    co = s.country;
                if(co)
                {
                    Rect iconRect = new Rect(selectionRect.position, Vector2.one * 16);
                    EditorGUI.DrawRect(iconRect, selected ? selectedColor : backgroundColor);
                    EditorGUI.LabelField(iconRect, new GUIContent(co.flag));
                }
            }
        }
    }
}

[CustomPropertyDrawer(typeof(DrawIfAttribute))]
public class DrawIfAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        VoiceAttribute voice = attribute as VoiceAttribute;

        DrawIfAttribute at = attribute as DrawIfAttribute;
        SerializedProperty boolProp = property.serializedObject.FindProperty(at.boolRequired);
        if (boolProp.boolValue)
            EditorGUI.PropertyField(position, property);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        DrawIfAttribute at = attribute as DrawIfAttribute;
        bool active = property.serializedObject.FindProperty(at.boolRequired).boolValue;
            return active ? 18 : 0;
    }
}

[CustomPropertyDrawer(typeof(VoiceAttribute))]
public class VoiceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
#if !RTVOICE
        EditorGUI.LabelField(position, property.stringValue + " (RT Voice is disabled)");
        return;
#endif

        if (!Speaker.isMaryMode)
        {
            EditorGUI.LabelField(position, property.stringValue + " (MaryTTS mode required)");
            return;
        }

        VoiceAttribute voice = attribute as VoiceAttribute;

        List<string> voices = new List<string>();
        for (int i = 0; i < Speaker.voiceProvider.Voices.Count; i++)
        {
            Crosstales.RTVoice.Model.Voice v = Speaker.voiceProvider.Voices[i];
            voices.Add(v.Name);
            if (voices[i] == property.stringValue)
                voice.voiceIndex = i;
        }

        voice.voiceIndex = EditorGUI.Popup(position, voice.voiceIndex, voices.ToArray());
        voice.voiceName = voices[voice.voiceIndex];
        property.stringValue = voice.voiceName;
    }
}

[CustomPropertyDrawer(typeof(CharacterAttribute))]
public class CharacterDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        VoiceAttribute voice = attribute as VoiceAttribute;

        if (Application.isPlaying || property.serializedObject.isEditingMultipleObjects)
        {
            EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Character), true);
        }
        else
        {
            Character[] characters = GameObject.FindObjectsOfType<Character>();
            string[] characterNames = new string[characters.Length];
            int currentIndex = 0;
            for (int i = 0; i < characters.Length; i++)
            {
                Character c = characters[i];
                characterNames[i] = c.name;
                if (property.objectReferenceValue == c)
                {
                    currentIndex = i;
                }
            }
            currentIndex = EditorGUI.Popup(position, label.text, currentIndex, characterNames);

            property.objectReferenceValue = characters[currentIndex];
        }
    }
}

#endif

class CharacterAttribute : PropertyAttribute
{
}

class VoiceAttribute : PropertyAttribute
{
    public string voiceName;
    public int voiceIndex;
}

public class DrawIfAttribute : PropertyAttribute
{
    public string boolRequired;
    public DrawIfAttribute(string boolRequired)
    {
        this.boolRequired = boolRequired;
    }
}