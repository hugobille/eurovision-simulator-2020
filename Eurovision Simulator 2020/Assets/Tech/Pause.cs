﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : GameEvent
{
    public float seconds;

    public override IEnumerator Run()
    {
        yield return new WaitForSeconds(seconds);
    }

    void OnValidate()
    {
        gameObject.name = "Pause (" + seconds + ")";
    }
}
