﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Manager : MonoBehaviour
{
    public Country[] countries;
    public SpeechBubble speech;
    public GameObject atlas;
    public Canvas scoringGUI;
    public Canvas stageGUI;
    public Canvas introCanvas;
    public RawImage stageBackground;
    public RawImage presenter;
    public RawImage background;
    public RawImage countryFlag;
    public Text countryLabel;
    public FXManager fxManager;
    public Transform songHolder;

    public Transform cornerTalkPointRight;
    public Transform cornerTalkPointLeft;
    public Transform scoreTalkPoint;

    public string emphasis;

    public static bool skipMusic;
    public static Song testSong;
    public static Country testCountry;
    public YoutubePlayer.YoutubePlayer youtubeScreen;

    [Header("Demo Settings")]
    public Country demoCountry;
    public Song demoSong;
    public Cutscene demoCutscene;

    public static Manager instance
    {
        get {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return FindObjectOfType<Manager>();
#endif
            return _instance;
        }
    }
    static Manager _instance;

    List<Song> songs;
    public bool youtubeStarting { get; set; }
    bool paused;

    void Awake()
    {
        _instance = this;
        songs = songHolder.GetComponentsInChildren<Song>().ToList();
    }

    public void DemoCutscene()
    {
        Cutscene.testCutscene = demoCutscene;
        StartCoroutine(Test());
    }

    public void DemoScoring()
    {
        testCountry = demoCountry;
        StartCoroutine(Test());
    }

    public void DemoSong()
    {
        testSong = demoSong;
        StartCoroutine(Test());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            Time.timeScale = paused ? 1 : 0;
        }
        youtubeScreen.GetComponent<UnityEngine.Video.VideoPlayer>().playbackSpeed = Time.timeScale;
    }

    bool runProgram = false;

    IEnumerator Test()
    {
        runProgram = false;
        yield return StartCoroutine(Start());
        runProgram = true;
    }

    IEnumerator Start()
    {
        introCanvas.gameObject.SetActive(false);

        for (int i = 0; i < songs.Count; i++)
        {
            songs[i].transform.parent = scoringGUI.transform.Find("Songs");
            songs[i].change.text = "";
            songs[i].songName.text = songs[i].name;
            StartCoroutine(songs[i].Move(songs[i].transform.GetSiblingIndex(), 0));
            songs[i].standing = songs[i].transform.GetSiblingIndex();
        }
        countries = atlas.GetComponentsInChildren<Country>();
        Shuffler.Shuffle(countries);

        if (testSong != null)
        {
            yield return PlaySong(testSong);
            testSong = null;
        }

        if (Cutscene.testCutscene != null)
        {
            yield return Cutscene.testCutscene.Play();
            Cutscene.testCutscene = null;
        }
        
        if (testCountry != null)
        {
            yield return AwardPointsRoutine(testCountry);
            testCountry = null;
        }
        if (runProgram)
            yield return StartCoroutine(Program());
        else
        {
            introCanvas.gameObject.SetActive(true);
            fxManager.Confetti();
        }
    }

    IEnumerator Program()
    {
        yield return StartCoroutine(PlayMusic());
        yield return StartCoroutine(AwardPoints());
    }

    IEnumerator PlayMusic()
    {
        scoringGUI.GetComponent<CanvasGroup>().alpha = 0;
        foreach(Song s in songs)
        {
            yield return StartCoroutine(PlaySong(s));
        }
    }

    IEnumerator PlaySong(Song s)
    {
        scoringGUI.GetComponent<CanvasGroup>().alpha = 0;
        speech.ShowDialog("And now it's time for: " + s.artist + " from " + s.country.name + " with " + emphasis + " " + s.name + "</prosody></prosody>", cornerTalkPointRight);
        yield return new WaitForSeconds(7);
        fxManager.Confetti();
        youtubeStarting = false;
        PlayYoutube(s.weblink);
        while (!youtubeStarting)
            yield return null;
        yield return new WaitForSeconds(15); //max load time
        while (youtubeScreen.GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying)
            yield return null;
        youtubeScreen.gameObject.SetActive(false);
    }

    public async void PlayYoutube(string url)
    {
        youtubeScreen.gameObject.SetActive(true);
        youtubeScreen.YoutubeVideoStarting += StartingVideo;
        await youtubeScreen.PlayVideoAsync(url);
    }

    void StartingVideo(string url)
    {
        youtubeStarting = true;
        youtubeScreen.YoutubeVideoStarting -= StartingVideo;
    }

    IEnumerator AwardPoints()
    {
        scoringGUI.GetComponent<CanvasGroup>().alpha = 1;
        foreach (Country c in countries)
        {
            yield return StartCoroutine(AwardPointsRoutine(c));
        }
    }

    IEnumerator AwardPointsRoutine(Country c)
    {
        foreach (Song s in songs)
        {
            s.change.text = "";
        }
        scoringGUI.GetComponent<CanvasGroup>().alpha = 1;
        SetPresenter(c);
        int randomCharacter = Random.Range(0, Character.allCharacters.Count);
        speech.SetVoice(Character.allCharacters[randomCharacter].voice);
        speech.ShowDialog("Good evening " + c.capital + "!", cornerTalkPointLeft);
        yield return new WaitForSeconds(3);
        speech.SetVoice(c.voice, c.speedOffset, c.pitchOffset);
        speech.ShowDialog("Thank you for a fantastic show!", scoreTalkPoint);
        yield return new WaitForSeconds(3);
        List<Song> songsLeft = new List<Song>();
        songsLeft.AddRange(songs);
        foreach (Song s in songs)
        {
            if (songsLeft.Contains(s) && s.country == c) //cannot vote for your own country
                songsLeft.Remove(s);
        }
        speech.ShowDialog("Here is the result of the " + c.adjective + " vote!", scoreTalkPoint);
        yield return new WaitForSeconds(2);
        for (int i = 1; i <= 8; i++)
        {
            Song so = songsLeft[Random.Range(0, songsLeft.Count)];
            so.pts += i;
            so.change.text = "+" + i;
            so.points.text = so.pts.ToString();
            songsLeft.Remove(so);
        }
        Sort();
        yield return new WaitForSeconds(3);
        speech.ShowDialog("And our nine points goes to...", scoreTalkPoint);
        yield return new WaitForSeconds(3);
        Song son = songsLeft[Random.Range(0, songsLeft.Count)];
        speech.ShowDialog(son.country.name + ", " + son.songName.text + "!", scoreTalkPoint);
        son.pts += 9;
        son.change.text = "+" + 9;
        son.points.text = son.pts.ToString();
        songsLeft.Remove(son);
        Sort();
        yield return new WaitForSeconds(3);
        speech.ShowDialog("And our ten points goes to...", scoreTalkPoint);
        yield return new WaitForSeconds(3);
        son = songsLeft[Random.Range(0, songsLeft.Count)];
        speech.ShowDialog(son.country.name + ", " + son.songName.text + "!", scoreTalkPoint);
        son.pts += 10;
        son.change.text = "+" + 10;
        son.points.text = son.pts.ToString();
        songsLeft.Remove(son);
        Sort();
        yield return new WaitForSeconds(3);
        speech.ShowDialog("And our twelve points goes to...", scoreTalkPoint);
        yield return new WaitForSeconds(5);
        son = songsLeft[Random.Range(0, songsLeft.Count)];
        speech.ShowDialog(son.country.name + ", " + son.songName.text + "!", scoreTalkPoint);
        son.pts += 12;
        son.change.text = "+" + 12;
        son.points.text = son.pts.ToString();
        songsLeft.Remove(son);
        Sort();
        yield return new WaitForSeconds(3);
        //            while (!Input.GetKeyDown(KeyCode.Space))
        //                yield return null;
        speech.SetVoice(Character.allCharacters[randomCharacter].voice);
        speech.ShowDialog("Thank you " + c.capital + "!", cornerTalkPointLeft);
        yield return new WaitForSeconds(2);
    }

    public void SetPresenter(Country c)
    {
        presenter.texture = c.presenter;
        presenter.transform.localScale = c.presenterScale;
        presenter.transform.localPosition = c.presenterPosition;
        background.texture = c.background;
        background.transform.localScale = c.backgroundScale;
        background.transform.localPosition = c.backgroundPosition;
        countryFlag.texture = c.flag;
        countryLabel.text = c.name;
    }

    void Sort()
    {
        songs = songs.OrderBy(x => -x.pts).ToList();
        for(int i = 0; i < songs.Count; i++)
        {
            StartCoroutine(songs[i].Move(i, 0.5f));
        }
    }
}

public static class Shuffler
{
    static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
