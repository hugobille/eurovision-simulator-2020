﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;


[CustomEditor(typeof(Song))]
[CanEditMultipleObjects]
public class SongEditor : Editor
{
    string oldName;
    int oldOrder = -1;

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        Song mainSong = target as Song;
        if (mainSong.transform.GetSiblingIndex() != oldOrder || target.name != oldName || EditorGUI.EndChangeCheck())
        {
            oldOrder = mainSong.transform.GetSiblingIndex();
            oldName = target.name;
            foreach (Object o in targets)
            {
                Song s = o as Song;
                s.flag.texture = s.country.flag;
                s.songName.text = s.name;
            }
            foreach (Song s in FindObjectsOfType<Song>())
            {
                s.StartCoroutine(s.Move(s.transform.GetSiblingIndex(), 0));
                EditorUtility.SetDirty(s.gameObject);
                EditorUtility.SetDirty(s.transform);
                EditorUtility.SetDirty(s.flag);
                EditorUtility.SetDirty(s.songName);
            }
        }
        if (GUILayout.Button("Test Play"))
        {
            Manager.testSong = target as Song;
            EditorApplication.isPlaying = true;
        }
    }
}
#endif

public class Song : MonoBehaviour
{
    public Country country;
    public string artist;
    public string weblink;

    [Header("Temp")]
    [HideInInspector]
    public int standing;
    [HideInInspector]
    public int pts;

    [Header("Commons")]
    [HideInInspector]
    public float verticalSpace = 1;
    [HideInInspector]
    public AnimationCurve sort;
    [HideInInspector]
    public Text points;
    [HideInInspector]
    public Text change;
    [HideInInspector]
    public Text index;
    [HideInInspector]
    public Text songName;
    [HideInInspector]
    public RawImage flag;
    [HideInInspector]
    public AnimationCurve sortMovement;
    [HideInInspector]
    public AnimationCurve sortScale;
    [HideInInspector]
    public Vector3 offset;


    public IEnumerator Move(int newStanding, float duration)
    {
        int oldStanding = standing;
        standing = newStanding;
        float t = 0;
        int oldColumn = 0;
        int newColumn = 0;
        if(oldStanding >= 13)
        {
            oldColumn = 1;
            oldStanding -= 13;
        }
        if (newStanding >= 13)
        {
            newColumn = 1;
            newStanding -= 13;
        }
        Vector3 oldPos = Vector3.down * ((oldStanding * verticalSpace)) + Vector3.right * (oldColumn == 0 ? 127 : 354);
        Vector3 newPos = Vector3.down * ((newStanding * verticalSpace)) + Vector3.right * (newColumn == 0 ? 127 : 354);
        oldPos += offset;
        newPos += offset;
        if (duration != 0)
        {
            while (t < 1)
            {
                transform.localPosition = Vector3.Lerp(oldPos, newPos, sortMovement.Evaluate(t));
                if (standing < oldStanding)
                {
                    transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.1f, sortScale.Evaluate(t));
                    transform.SetSiblingIndex(transform.parent.childCount);
                }
                t += Time.deltaTime / duration;
                yield return null;
            }
        }
        transform.localScale = Vector3.one;
        transform.localPosition = newPos;
    }
}
