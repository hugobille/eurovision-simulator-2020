﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : GameEvent
{
    [Character]
    public Character character;
    [Range(-1.5f, 1.5f)]
    public float position;
    public float duration;
    public bool waitForFinish;

    [Header("Animation")]
    [Range(0, 1)]
    public float bouncy;
    [Range(0, 1)]
    public float wobbly;
    [Range(0.01f, 1)]
    public float speed = 0.2f;

    public override IEnumerator Run()
    {
        if (waitForFinish)
            yield return StartCoroutine(Go());
        else
            StartCoroutine(Go());
    }

    IEnumerator Go()
    {
        if (bouncy != 0 || wobbly != 0)
            character.Animate(bouncy, wobbly, speed);
        float t = 0;
        Vector3 startPos = character.transform.localPosition;
        Vector3 endPos = Vector3.right * position * 400 + Vector3.down * 100;
        while (t < 1)
        {
            character.transform.localPosition = Vector3.Lerp(startPos, endPos, t);
            t += Time.deltaTime / duration;
            yield return null;
        }
        if (bouncy != 0 || wobbly != 0)
            character.Animate(0,0,0);
    }

    void OnValidate()
    {
        gameObject.name = "Move " + character.name;
    }
}
