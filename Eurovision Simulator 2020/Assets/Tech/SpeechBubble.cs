﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RedBlueGames.Tools.TextTyper;
#if RTVOICE
using Crosstales.RTVoice;
#endif

public class SpeechBubble : MonoBehaviour {

    public TextTyper typer;
    public float offset;
    public Canvas canvas;
    public AudioSource audioSource;
    public string voice;

    float speedOffset;
    float pitchOffset;

    Transform speakPoint;

    public void SetVoice(string voice, float speed = 0, float pitch = 0)
    {
        pitchOffset = pitch;
        speedOffset = speed;
        this.voice = voice;
    }

    private void Update()
    {
        if (speakPoint != null)
            transform.position = speakPoint.position + Vector3.up * offset;
        else
            transform.position = Vector3.zero;
    }

    public Coroutine ShowDialog(string text, Transform speakPoint)
    {
        if(speedOffset != 0)
        {
            text = text.Insert(0, "<prosody rate=" + "\"" + (speedOffset * 1000) + "%" + "\"" + ">");
            text += "</prosody>";
        }
        if (pitchOffset != 0)
        {
            text = text.Insert(0, "<prosody pitch=" + "\"" + (pitchOffset * 200) + "%" + "\"" + ">");
            text += "</prosody>";
        }
        canvas.gameObject.SetActive(true);
        StopAllCoroutines();
#if RTVOICE
        Speaker.Speak(text, audioSource, Speaker.VoiceForName(voice));
#endif
        this.speakPoint = speakPoint;
        return StartCoroutine(ShowDialogRoutine(text));
    }

    IEnumerator ShowDialogRoutine(string text)
    {
        text = CleanText(text);
        typer.TypeText(text);

        while (!typer.IsCompleted)
        {
            yield return null;
        }
        yield return null;
        yield return new WaitForSeconds(1);
        canvas.gameObject.SetActive(false);
    }

    string CleanText(string input)
    {
        string[] parts = input.Split(new char[] { '<', '>' });
        string clean = "";
        for(int i = 0; i < parts.Length; i += 2)
        {
            //include every second text chunk
            clean += parts[i];
        }
        return clean;
    }
}
