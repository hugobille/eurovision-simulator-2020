﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStage : GameEvent
{
    [System.Serializable]
    public class CharacterSetting
    {
        [Character]
        public Character character;
        [Range(-1.5f,1.5f)]
        public float position;
    }
    public Texture2D background;
    public CharacterSetting[] characters;
    

    public override IEnumerator Run()
    {
        Manager.instance.stageBackground.texture = background;
        Character.ClearAll();
        foreach(CharacterSetting s in characters)
        {
            s.character.gameObject.SetActive(true);
            s.character.transform.localPosition = Vector3.right * s.position * 400 + Vector3.down * 100;
        }
        yield break;
    }

    void OnValidate()
    {
        gameObject.name = "Set Stage";
    }

}
