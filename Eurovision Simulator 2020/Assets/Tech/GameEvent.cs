﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameEvent), true), CanEditMultipleObjects]
public class GameEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Test Cutscene"))
        {
            Cutscene c = (target as GameEvent).GetComponentInParent<Cutscene>();
            Cutscene.testCutscene = c;
            EditorApplication.isPlaying = true;
        }
    }
}
#endif

public abstract class GameEvent : MonoBehaviour
{
    public abstract IEnumerator Run();
    
}
