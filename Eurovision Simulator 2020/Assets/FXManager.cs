﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour
{
    public ParticleSystem confettiLeft;
    public ParticleSystem confettiRight;

    public void Confetti()
    {
        confettiLeft.Play();
        confettiRight.Play();
    }
}
