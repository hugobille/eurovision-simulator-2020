﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Cutscene))]
public class CutsceneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Test"))
        {
            Cutscene.testCutscene = target as Cutscene;
            EditorApplication.isPlaying = true;
        }
    }
}
#endif


public class Cutscene : MonoBehaviour
{

    public static Cutscene testCutscene;
    GameEvent[] events;

    void Awake()
    {
        events = GetComponentsInChildren<GameEvent>();
    }

    public Coroutine Play()
    {
        Manager.instance.scoringGUI.gameObject.GetComponent<CanvasGroup>().alpha = 0;
        return StartCoroutine(RunRoutine());
    }

    IEnumerator RunRoutine()
    {
        foreach(GameEvent e in events)
        {
            yield return e.Run();
        }
        Manager.instance.stageGUI.GetComponent<CanvasGroup>().alpha = 0;
        Character.ClearAll();
    }
}
