﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Crosstales.RTVoice;
#if UNITY_EDITOR
using UnityEditor;


[CustomEditor(typeof(Country))]
[CanEditMultipleObjects]
public class CountryEditor : Editor
{
    Object oldTarget;

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (oldTarget != target || EditorGUI.EndChangeCheck())
        {
            oldTarget = target;
            Manager.instance.SetPresenter(target as Country);
            EditorUtility.SetDirty(Manager.instance);
        }
        if (GUILayout.Button("Test Award Points"))
        {
            Manager.testCountry = target as Country;
            EditorApplication.isPlaying = true;
        }
    }
}

#endif

public class Country : MonoBehaviour
{
    public string capital;
    public string adjective;
    public Texture2D flag;
    [Header("Voice")]
    [Voice]
    public string voice = "Microsoft Hazel Desktop";
    [Range(-5,5)]
    public float pitchOffset;
    [Range(-5, 5)]
    public float speedOffset;
    [Header("Background")]
    public Texture2D background;
    public Vector2 backgroundScale = Vector3.one;
    public Vector2 backgroundPosition = Vector3.zero;
    [Header("Presenter")]
    public Texture2D presenter;
    public Vector2 presenterScale = Vector3.one;
    public Vector2 presenterPosition = Vector3.zero;
}