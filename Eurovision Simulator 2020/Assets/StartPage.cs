﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartPage : MonoBehaviour
{
    public Text days;
    public Text hours;
    public Text minutes;
    public Text seconds;
    public Vector4 startTime;

    System.DateTime endTime;

    void Start()
    {
        endTime = new System.DateTime(2020, (int)startTime.x, (int)startTime.y, (int)startTime.z, (int)startTime.w, 0);
    }

    void Update()
    {
        System.TimeSpan ts = endTime.Subtract(System.DateTime.Now);
        days.text = ts.Days.ToString();
        hours.text = ts.Hours.ToString();
        minutes.text = ts.Minutes.ToString();
        seconds.text = ts.Seconds.ToString();
    }
}
