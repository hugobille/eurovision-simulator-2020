﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joke : GameEvent
{
    public enum Intonation
    {
        neutral,
        upwards,
        downwards
    }

    [Multiline(5)]
    public string text;
    [Character]
    public Character character;
    public bool waitForFinish = true;
    public float delaySeconds = 0;
    [Header("Speech")]
    [Range(-1,1)]
    public float talkSpeed = 0;
    [Range(-1, 1)]
    public float talkPitch = 0;
    [Range(0, 2)]
    public float talkVolume = 0;
    public Intonation intonation;
    public bool customMelody;
    [DrawIf("customMelody")]
    public AnimationCurve melody = new AnimationCurve(new Keyframe[] { new Keyframe(0, -1), new Keyframe(1, 1) });
    [DrawIf("customMelody")]
    public int detail = 10;
    [Header("Animation")]
    [Range(0, 1)] public float bouncy;
    [Range(0, 1)] public float wobbly;
    [Range(0.01f, 1)] public float speed = 0.2f;

    private void Start()
    {
        if (customMelody)
        {
            int lastLength = 0;
            int originalTextLength = text.Length;
            for (int i = 0; i < detail; i++)
            {
                float val = (talkPitch + character.pitchOffset + melody.Evaluate((float)i / (float)detail)) * 200;
                string str = "<prosody pitch =" + "\"" + val + "%" + "\"" + ">";
                if (i != 0)
                    str = str.Insert(0, "</prosody>");
                int index = (originalTextLength / detail) * i;
                index += lastLength;
                while (text[index] != ' ' && index != 0)
                    index--;
                text = text.Insert(index, str);
                lastLength += str.Length;
            }
            text += ("</prosody>");
        }
        if (talkSpeed != 0 || character.speedOffset != 0)
        {
            float percent = talkSpeed * 2000;
            text = text.Insert(0, "<prosody rate =" + "\"" + percent + "%" + "\"" + ">");
            text += ("</prosody>");
        }
        if (talkPitch != 0 || character.pitchOffset != 0)
        {
            float percent = talkPitch + character.pitchOffset *200;
            text = text.Insert(0, "<prosody pitch =" + "\"" + percent + "%" + "\"" + ">");
            text += ("</prosody>");
        }
        if (talkVolume != 1)
        {
            float percent = talkVolume * 100;
            text = text.Insert(0, "<prosody volume =" + "\"" + percent + "%" + "\"" + ">");
            text += ("</prosody>");
        }
        
        if (intonation == Intonation.upwards)
        {
            text = text.Insert(0, "<prosody contour =" + "\"" + "(0%,+20%) (40%,+40%) (60%,+60%) (80%,+80%) (100%,+100%)" + "\"" + ">");
            text += ("</prosody>");
        }
        else if(intonation == Intonation.downwards)
        {
            text = text.Insert(0, "<prosody contour =" + "\"" + "(0%,-20%) (40%,-40%) (60%,-60%) (80%,-80%) (100%,-100%)" + "\"" + ">");
            text += ("</prosody>");
        }


    }

    public override IEnumerator Run()
    {
        if(bouncy != 0 || wobbly != 0)
            character.Animate(bouncy, wobbly, speed);
        Manager.instance.speech.SetVoice(character.voice);
        if (waitForFinish)
            yield return Manager.instance.speech.ShowDialog(text, character.transform.Find("SpeakPoint"));
        else
            Manager.instance.speech.ShowDialog(text, character.transform.Find("SpeakPoint"));
        yield return new WaitForSeconds(delaySeconds);
        if (bouncy != 0 || wobbly != 0)
            character.Animate(0, 0, 0);
    }

    void OnValidate()
    {
        if(character.name != null)
            gameObject.name = character.name + " talks";
    }
}
