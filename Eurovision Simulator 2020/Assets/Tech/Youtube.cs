﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Youtube : GameEvent
{
    public string url;
    public bool keepCharactersInView;
    public bool waitForFinish;

    public override IEnumerator Run()
    {
        if(waitForFinish)
            yield return StartCoroutine(Play());
        else
            StartCoroutine(Play());
    }

    IEnumerator Play()
    {
        if (!keepCharactersInView)
            Character.ClearAll();
        Manager.instance.PlayYoutube(url);
        while (!Manager.instance.youtubeStarting)
            yield return null;
        yield return new WaitForSeconds(8); //max load time
        while (Manager.instance.youtubeScreen.GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying)
            yield return null;
        Manager.instance.youtubeScreen.gameObject.SetActive(false);
    }

    void OnValidate()
    {
        gameObject.name = "Youtube";
    }
}



